package ru.t1.strelcov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Setter
@Getter
@NoArgsConstructor
public final class ServerVersionResponse extends AbstractResultResponse {

    @Nullable
    private String version;

    public ServerVersionResponse(@Nullable final String version) {
        this.version = version;
    }

}
