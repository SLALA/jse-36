package ru.t1.strelcov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.strelcov.tm.api.repository.IProjectRepository;
import ru.t1.strelcov.tm.api.service.IProjectService;
import ru.t1.strelcov.tm.enumerated.SortType;
import ru.t1.strelcov.tm.enumerated.Status;
import ru.t1.strelcov.tm.exception.AbstractException;
import ru.t1.strelcov.tm.model.Project;
import ru.t1.strelcov.tm.repository.ProjectRepository;
import ru.t1.strelcov.tm.util.CheckUtil;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Vector;
import java.util.stream.Collectors;

public class ProjectServiceTest {

    @NotNull
    private final IProjectRepository repository = new ProjectRepository();

    @NotNull
    private final IProjectService service = new ProjectService(repository);


    @NotNull
    private static final String USER_ID_UNIQUE = "USER_ID_UNIQUE";

    @NotNull
    private static final String NAME_UNIQUE = "NAME_UNIQUE";

    @NotNull
    private final Project[] PROJECTS = new Project[]{
            new Project("id2", "pr2"),
            new Project("id1", "pr0"),
            new Project("id0", "pr1"),
            new Project("id2", "pr8"),
            new Project("id1", "pr3"),
            new Project("id3", "pr6")
    };

    @Before
    public void before() {
        repository.addAll(Arrays.asList(PROJECTS));
    }

    @Test
    public void addTest() {
        @NotNull final Project project = new Project("id1", "pr1");
        int size = service.findAll().size();
        service.add(null);
        Assert.assertEquals(size, service.findAll().size());
        service.add(project);
        Assert.assertEquals(size + 1, service.findAll().size());
        Assert.assertTrue(service.findAll().contains(project));
    }

    @Test
    public void addAllTest() {
        @NotNull final Project[] newProjects = new Project[]{new Project("id11", "pr11"), new Project("id22", "pr22")};
        @NotNull final Vector<Project> list = new Vector<>(Arrays.asList(newProjects));
        int size = service.findAll().size();
        int listSize = list.size();
        @NotNull final Vector<Project> listWithNulls = new Vector<>(list);
        listWithNulls.addAll(Arrays.asList(null, null));
        service.addAll(listWithNulls);
        Assert.assertEquals(size + listSize, service.findAll().size());
        Assert.assertTrue(service.findAll().containsAll(list));
    }

    @Test
    public void findAllTest() {
        Assert.assertEquals(Arrays.asList(PROJECTS).size(), service.findAll().size());
        Assert.assertTrue(service.findAll().containsAll(Arrays.asList(PROJECTS)));
    }

    @Test
    public void clearTest() {
        Assert.assertNotEquals(0, service.findAll().size());
        service.clear();
        Assert.assertEquals(0, service.findAll().size());
    }

    @Test
    public void removeTest() {
        @NotNull final Project project = service.findAll().get(0);
        int fullSize = service.findAll().size();
        service.remove(project);
        Assert.assertFalse(service.findAll().contains(project));
        Assert.assertEquals(fullSize - 1, service.findAll().size());
    }

    @Test
    public void findByIdTest() {
        Assert.assertThrows(AbstractException.class, () -> service.findById(null));
        Assert.assertThrows(AbstractException.class, () -> service.findById("notExistedId"));
        for (@NotNull final Project project : service.findAll()) {
            Assert.assertTrue(service.findAll().contains(service.findById(project.getId())));
            Assert.assertEquals(project, service.findById(project.getId()));
        }
    }

    @Test
    public void removeByIdTest() {
        Assert.assertThrows(AbstractException.class, () -> service.removeById(null));
        Assert.assertThrows(AbstractException.class, () -> service.removeById("notExistedId"));
        @NotNull final Project project = service.findAll().get(0);
        int fullSize = service.findAll().size();
        Assert.assertNotNull(service.removeById(project.getId()));
        Assert.assertFalse(service.findAll().contains(project));
        Assert.assertEquals(fullSize - 1, service.findAll().size());
    }

    @Test
    public void updateByIdByUserIdTest() {
        for (@NotNull final Project project : service.findAll()) {
            if (CheckUtil.isEmpty(project.getUserId())) continue;
            @NotNull final String userId = project.getUserId();
            @NotNull final String id = project.getId();
            Assert.assertThrows(AbstractException.class, () -> service.updateById(userId, null, "newName", "newDesc"));
            Assert.assertThrows(AbstractException.class, () -> service.updateById(null, id, "newName", "newDesc"));
            service.updateById(userId, id, "newName", "newDesc");
            Assert.assertEquals("newName", service.findById(userId, id).getName());
            Assert.assertEquals("newDesc", service.findById(userId, id).getDescription());
            Assert.assertNotNull(service.updateById(userId, id, null, null));
            Assert.assertNull(service.findById(userId, id).getName());
            Assert.assertNull(service.findById(userId, id).getDescription());
        }
    }

    @Test
    public void updateByNameByUserIdTest() {
        for (@NotNull final Project project : service.findAll()) {
            if (CheckUtil.isEmpty(project.getUserId()) || CheckUtil.isEmpty(project.getName())) continue;
            @NotNull final String userId = project.getUserId();
            @NotNull final String name = project.getName();
            @Nullable final String newName = "newName";
            Assert.assertThrows(AbstractException.class, () -> service.updateByName(userId, null, newName, "newDesc"));
            Assert.assertThrows(AbstractException.class, () -> service.updateByName(null, name, newName, "newDesc"));
            service.updateByName(userId, name, newName, "newDesc");
            Assert.assertEquals(project, service.findByName(userId, newName));
            Assert.assertEquals("newDesc", service.findByName(userId, newName).getDescription());
            Assert.assertNotNull(service.updateByName(userId, newName, null, null));
        }
    }

    @Test
    public void changeStatusByIdByUserIdTest() {
        for (@NotNull final Project project : service.findAll()) {
            if (CheckUtil.isEmpty(project.getUserId())) continue;
            @NotNull final String userId = project.getUserId();
            @NotNull final String id = project.getId();
            for (@NotNull final Status status : Status.values()) {
                Assert.assertThrows(AbstractException.class, () -> service.changeStatusById(null, id, status));
                Assert.assertThrows(AbstractException.class, () -> service.changeStatusById(userId, null, status));
                service.changeStatusById(userId, id, status);
                Assert.assertEquals(status, service.findById(userId, id).getStatus());
            }
        }
    }

    @Test
    public void changeStatusByNameByUserIdTest() {
        for (@NotNull final Project project : service.findAll()) {
            if (CheckUtil.isEmpty(project.getUserId()) || CheckUtil.isEmpty(project.getName())) continue;
            @NotNull final String userId = project.getUserId();
            @NotNull final String name = project.getName();
            for (@NotNull final Status status : Status.values()) {
                Assert.assertThrows(AbstractException.class, () -> service.changeStatusByName(null, name, status));
                Assert.assertThrows(AbstractException.class, () -> service.changeStatusByName(userId, null, status));
                service.changeStatusByName(userId, name, status);
                Assert.assertEquals(status, service.findByName(userId, name).getStatus());
            }
            Assert.assertThrows(AbstractException.class, () -> service.changeStatusByName(userId, null, Status.IN_PROGRESS));
            Assert.assertThrows(AbstractException.class, () -> service.changeStatusByName(userId, null, Status.IN_PROGRESS));
        }
    }

    @Test
    public void findAllByUserIdTest() {
        @NotNull final Project[] newProjects = new Project[]{new Project(USER_ID_UNIQUE, "pr2"), new Project(USER_ID_UNIQUE, "pr1")};
        Assert.assertThrows(AbstractException.class, () -> service.findAll(null));
        @NotNull final Vector<Project> list = new Vector<>(Arrays.asList(newProjects));
        @Nullable String userId = list.get(0).getUserId();
        Assert.assertEquals(0, service.findAll(userId).size());
        service.addAll(list);
        Assert.assertEquals(list.size(), service.findAll(userId).size());
        Assert.assertTrue(list.containsAll(service.findAll(userId)));

    }

    @Test
    public void findAllSortedByUserIdTest() {
        @NotNull final Project[] newProjects = new Project[]{new Project(USER_ID_UNIQUE, "pr2"), new Project(USER_ID_UNIQUE, "pr1")};
        @NotNull final Vector<Project> list = new Vector<>(Arrays.asList(newProjects));
        @NotNull Comparator<Project> comparator = SortType.NAME.getComparator();
        @Nullable String userId = list.get(0).getUserId();
        Assert.assertThrows(AbstractException.class, () -> service.findAll(null, comparator));
        Assert.assertEquals(0, service.findAll(userId).size());
        service.addAll(list);
        @NotNull final List<Project> sortedList = service.findAll(userId, comparator);
        Assert.assertEquals(list.size(), sortedList.size());
        Assert.assertTrue(list.containsAll(sortedList));
        Assert.assertEquals(sortedList.stream().sorted(comparator)
                .collect(Collectors.toList()), sortedList);
    }

    @Test
    public void findByNameByUserIdTest() {
        Assert.assertThrows(AbstractException.class, () -> service.findByName(null, "notExistedId"));
        Assert.assertThrows(AbstractException.class, () -> service.findByName("notExistedId", null));
        Assert.assertThrows(AbstractException.class, () -> service.findByName("notExistedId", "notExistedId"));
        for (@NotNull final Project project : service.findAll()) {
            if (CheckUtil.isEmpty(project.getUserId()) || CheckUtil.isEmpty(project.getName())) continue;
            @NotNull final String userId = project.getUserId();
            @NotNull final String name = project.getName();
            Assert.assertTrue(service.findAll().contains(service.findByName(userId, name)));
            Assert.assertEquals(project.getName(), service.findByName(userId, name).getName());
            Assert.assertThrows(AbstractException.class, () -> service.findByName("notExistedId", name));
            Assert.assertThrows(AbstractException.class, () -> service.findByName(userId, "notExistedId"));
        }
    }

    @Test
    public void removeByNameByUserIdTest() {
        Assert.assertThrows(AbstractException.class, () -> service.removeByName(null, "notExistedId"));
        Assert.assertThrows(AbstractException.class, () -> service.removeByName("notExistedId", null));
        @NotNull final Project project = service.findAll().get(0);
        int fullSize = service.findAll().size();
        Assert.assertNotNull(service.removeByName(project.getUserId(), project.getName()));
        Assert.assertFalse(service.findAll().contains(project));
        Assert.assertEquals(fullSize - 1, service.findAll().size());
    }

    @Test
    public void findByIdByUserIdTest() {
        Assert.assertThrows(AbstractException.class, () -> service.findById(null, "notExistedId"));
        Assert.assertThrows(AbstractException.class, () -> service.findById("notExistedId", null));
        Assert.assertThrows(AbstractException.class, () -> service.findById("notExistedId", "notExistedId"));
        for (@NotNull final Project project : service.findAll()) {
            if (CheckUtil.isEmpty(project.getUserId())) continue;
            @NotNull final String userId = project.getUserId();
            Assert.assertTrue(service.findAll().contains(service.findById(userId, project.getId())));
            Assert.assertEquals(project, service.findById(userId, project.getId()));
            Assert.assertThrows(AbstractException.class, () -> service.findById("notExistedId", project.getId()));
            Assert.assertThrows(AbstractException.class, () -> service.findById(userId, "notExistedId"));
        }
    }

    @Test
    public void removeByIdByUserIdTest() {
        Assert.assertThrows(AbstractException.class, () -> service.removeById(null, "notExistedId"));
        Assert.assertThrows(AbstractException.class, () -> service.removeById("notExistedId", null));
        @NotNull final Project project = service.findAll().get(0);
        int fullSize = service.findAll().size();
        Assert.assertNotNull(service.removeById(project.getUserId(), project.getId()));
        Assert.assertFalse(service.findAll().contains(project));
        Assert.assertEquals(fullSize - 1, service.findAll().size());
    }

    @Test
    public void clearByUserIdTest() {
        final String userId = service.findAll().get(0).getUserId();
        Assert.assertThrows(AbstractException.class, () -> service.clear(null));
        @NotNull final List<Project> list = service.findAll(userId);
        int size = list.size();
        int fullSize = service.findAll().size();
        service.clear(userId);
        service.clear("notExistedId");
        Assert.assertEquals(0, service.findAll(userId).size());
        Assert.assertEquals(fullSize - size, service.findAll().size());
    }

    @Test
    public void createTest() {
        Assert.assertThrows(AbstractException.class, () -> service.add(null, "notExistedId", "descr_new"));
        Assert.assertThrows(AbstractException.class, () -> service.add("notExistedId", null, "descr_new"));
        int size = service.findAll().size();
        Assert.assertNotNull(service.add(USER_ID_UNIQUE, NAME_UNIQUE, "descr_new"));
        Assert.assertEquals(size + 1, service.findAll().size());
        Assert.assertTrue(service.findAll().contains(service.findByName(USER_ID_UNIQUE, NAME_UNIQUE)));
    }

}
