package ru.t1.strelcov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.strelcov.tm.api.repository.IUserRepository;
import ru.t1.strelcov.tm.enumerated.Role;
import ru.t1.strelcov.tm.model.User;

import java.util.Arrays;
import java.util.List;

public class UserRepositoryTest {

    @NotNull
    private final IUserRepository repository = new UserRepository();

    @NotNull
    private final User[] USERS = new User[]{
            new User("login1", "pass", Role.USER),
            new User("login2", "pass", Role.ADMIN),
            new User("login3", "pass", Role.USER),
            new User("login4", "pass", Role.USER),
            new User("login5", "pass", Role.ADMIN),
            new User("login6", "pass", Role.USER)
    };

    @Before
    public void before() {
        repository.addAll(Arrays.asList(USERS));
    }

    @Test
    public void addTest() {
        @NotNull User user = new User("id1", "pr4");
        int size = repository.findAll().size();
        Assert.assertFalse(repository.findAll().contains(user));
        repository.add(user);
        Assert.assertTrue(repository.findAll().contains(user));
        Assert.assertEquals(size + 1, repository.findAll().size());
    }

    @Test
    public void addAllTest() {
        @NotNull final List<User> list = Arrays.asList(new User("login1", "pr1"), new User("login2", "pr2"));
        int size = repository.findAll().size();
        Assert.assertFalse(repository.findAll().containsAll(list));
        repository.addAll(list);
        Assert.assertTrue(repository.findAll().containsAll(list));
        Assert.assertEquals(size + list.size(), repository.findAll().size());
    }

    @Test
    public void findAllTest() {
        Assert.assertEquals(Arrays.asList(USERS).size(), repository.findAll().size());
        Assert.assertTrue(repository.findAll().containsAll(Arrays.asList(USERS)));
    }

    @Test
    public void clearTest() {
        Assert.assertNotEquals(0, repository.findAll().size());
        repository.clear();
        Assert.assertEquals(0, repository.findAll().size());
    }

    @Test
    public void removeTest() {
        @NotNull final User user = repository.findAll().get(0);
        int fullSize = repository.findAll().size();
        repository.remove(user);
        Assert.assertFalse(repository.findAll().contains(user));
        Assert.assertEquals(fullSize - 1, repository.findAll().size());
    }

    @Test
    public void findByIdTest() {
        for (@NotNull final User user : repository.findAll()) {
            Assert.assertTrue(repository.findAll().contains(repository.findById(user.getId())));
            Assert.assertEquals(user, repository.findById(user.getId()));
        }
    }

    @Test
    public void removeByIdTest() {
        @NotNull final User user = repository.findAll().get(0);
        int fullSize = repository.findAll().size();
        Assert.assertNotNull(repository.removeById(user.getId()));
        Assert.assertFalse(repository.findAll().contains(user));
        Assert.assertEquals(fullSize - 1, repository.findAll().size());
    }

    @Test
    public void findByLogin() {
        for (@NotNull final User user : repository.findAll()) {
            Assert.assertTrue(repository.findAll().contains(repository.findByLogin(user.getLogin())));
        }
    }

    @Test
    public void removeByLogin() {
        @NotNull final User user = repository.findAll().get(0);
        int fullSize = repository.findAll().size();
        Assert.assertNotNull(repository.removeByLogin(user.getLogin()));
        Assert.assertFalse(repository.findAll().contains(user));
        Assert.assertEquals(fullSize - 1, repository.findAll().size());
    }

    @Test
    public void loginExists() {
        for (@NotNull final User user : repository.findAll()) {
            Assert.assertTrue(repository.loginExists(user.getLogin()));
        }
        Assert.assertFalse(repository.loginExists("testNotExistedLogin"));
    }

}
